package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Objects;



public class Polynomial{

    private ArrayList<Organ> poly = new ArrayList<Organ>();

    public Polynomial(ArrayList<Double> coefficients, ArrayList<Integer> exponents) throws Exception {

        if (!Objects.equals(coefficients.size(), exponents.size()))
            throw new Exception();

        Collections.sort(coefficients, Collections.reverseOrder());
        Collections.sort(exponents, Collections.reverseOrder());

        Iterator<Double> coeff = coefficients.iterator();
        Iterator<Integer> exp = exponents.iterator();

        while(coeff.hasNext() && exp.hasNext())
            poly.add(new Organ(coeff.next(), exp.next()));

    }

    @Override
    public String toString(){

        StringBuilder res = new StringBuilder();
        poly.forEach(organ -> res.append(organ));

        return String.valueOf(res);

    }

}
