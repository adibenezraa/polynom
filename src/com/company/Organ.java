package com.company;

public class Organ {

    private Double coefficient;
    private Integer exponent;

    public Organ(Double coefficient, Integer exponent){
        this.coefficient = coefficient;
        this.exponent = exponent;
    }

    @Override
    public String toString(){
        return coefficient + "," + exponent + " ";
    }
}
