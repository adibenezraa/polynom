package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class PolynomialOperations {

    private Polynomial p1, p2;

    PolynomialOperations() throws Exception {

        p1 = this.readPolynomialFromUser();
        p2 = this.readPolynomialFromUser();
    }

    @Override
    public String toString(){
        return p1.toString() + "\n" + p2.toString();
    }

    private Polynomial readPolynomialFromUser() throws Exception {

        ArrayList coefficients = new ArrayList<Double>();
        ArrayList exponents = new ArrayList<Integer>();
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please insert exponent and coeff");

        while (keyboard.hasNextDouble() && keyboard.hasNextInt()) {
            System.out.println("Please insert exponent and coeff");

            coefficients.add(keyboard.nextDouble());
            exponents.add(keyboard.nextInt());
        }

        return new Polynomial(coefficients, exponents);
    }

}
